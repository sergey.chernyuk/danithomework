let selectedServices = document.querySelector('.services-list .active');
const nameListConteiner = document.querySelector('.services-list');

let selectedWorkTipe = document.querySelector('.work-list .active');
const workListConteiner = document.querySelector('.work-list');

let avatarOrder = 0;

const avatarListConteiner = document.querySelector('.avatar-conteiner');
let selectedAvatar = document.querySelector('.avatar-conteiner .active')

let currentWorckCategory = document.querySelectorAll('.work-img-conteiner img');


const user = [{
  login: 'hasanAli',
  name:'Hasan Ali',
  position: 'UX Designer',
  review: 'The goal of any UI/UX review is to study goals, objectives, and behaviors to see if they align with the company\'s intended goals. ... A UX review is where an expert goes through a website looking for usability and experience problems and makes recommendations on how to fix them.',
},
{
  login: 'ammySweet',
  name:'Ammy Sweet',
  position: 'Web Design',
  review: 'It is a distinct pleasure for me to recommend Thrive Internet Marketing to any and all interested parties. They have been professional, comprehensive and competent throughout the process of our working together. We feel that we have established a relationship with them for years to come. The reaction to our new web site has been overwhelmingly positive; as one commented the site is \“FANTASTIC.\” The same can be said for our view of Thrive’s work for us.',
},
{
  login: 'mrProper',
  name:'Mr Proper',
  position: 'front-end',
  review: 'Since having our new website built by Thrive, we have seen a 200% increase in the number of online contact forms being filled out and returned to us. Matt and his team worked closely with us to provide a site that met all of the criteria that we were looking for. The end result was a website that is attractive, organized and effective. Thanks to Thrive for all of your hard work and support!',
},
{
  login: 'liziShwarc',
  name:'Lizi Shwarc',
  position: 'Designer',
  review: 'The individual review can be conducted in one of three ways, as an in-country review, a centralized review or a desk review.'
}
];


document.addEventListener('click', (event)=>{
  if (event.target.classList.contains("services-item")) switchServices(event.target);
  if (event.target.classList.contains("work-list-item")) switchWorkTabs(event.target);
  if(event.target.closest('.our-work-section .btn-more')) loadMoreImg(event.target, currentWorckCategory, document.querySelectorAll('.our-work-section .show'));
  if(event.target.closest('.avatar-small')){
    switchAvatar(event.target.closest('.avatar-small'));
    switchLargeAvatar(event.target.closest('.avatar-small'));
    switchReview(event.target.dataset.userName);
    };
  if(event.target.classList.contains("btn-arrow")){
    avatarOrder = selectedAvatar.firstElementChild.dataset.order;
    checkCarouselBtn(event.target.id)
    }
  if(event.target.closest('.gallery-of-best-images .btn-more')) {
  loadMoreImg(event.target, document.querySelectorAll('.masonry-brick'), document.querySelectorAll('.masonry .show'));
  masonry(".masonry", ".masonry-brick", 8, 3);
  }

  });

function switchServices(tab) {
  if (selectedServices)  selectedServices.classList.remove('active');

  selectedServices = tab;
  selectedServices.classList.add('active')
  
  document.querySelectorAll(`.services-tab-item`).forEach(element => {
   element.dataset.switchtext === selectedServices.dataset.switch ? element.classList.add('active') : element.classList.remove('active');
  });
}

function switchWorkTabs(tab) {
  selectedWorkTipe.classList.remove('active');
  selectedWorkTipe = tab;
  selectedWorkTipe.classList.add('active')
  const loadBtn = document.querySelector('.our-work-section .btn-more');

  document.querySelectorAll(`.work-img-conteiner img`).forEach(element => {
    if(selectedWorkTipe.dataset.switch === 'all'){
        element.classList.remove('hide');
        currentWorckCategory = document.querySelectorAll('.work-img-conteiner img');
    } else if( element.getAttribute('src').includes(`${selectedWorkTipe.dataset.switch}`)){
      element.classList.remove('hide');
      currentWorckCategory = document.querySelectorAll(`.work-img-conteiner [src*=${selectedWorkTipe.dataset.switch}]`);
    } else {
      element.classList.add('hide');
    }
  })
  trimm(currentWorckCategory)
 currentWorckCategory.length <= 12 ? loadBtn.classList.add('hide') : loadBtn.classList.remove('hide')
}

function trimm (velue) {
  [].forEach.call(velue, function(item, index){
      if (index > 12 - 1)  {
        item.classList.remove('show')// 12 - item step
        item.classList.add('hide')
      }
    })
} 

function loadMoreImg (btn, array, quantity) {
  const imgList = [...array];

  for (let i = quantity.length; i < quantity.length + 12; i++) {
      if (imgList[i])  imgList[i].classList.replace('hide', 'show');     
     }
  if (quantity.length + 12 >= imgList.length && btn.classList.contains("btn-more"))  btn.classList.add('hide');
} 


// Карусель
function switchAvatar(tab) {
    selectedAvatar.classList.remove('active')
    selectedAvatar = tab;
    selectedAvatar.classList.add('active')
}

function switchLargeAvatar(avatarlink){
  const avatarConteiner = document.querySelector('.avatar-large');
  avatarConteiner.classList.add('animate');

  setTimeout(function () {
   avatarConteiner.innerHTML = avatarlink.innerHTML ;
   avatarConteiner.classList.remove('animate')
  }, 400)
}

function switchReview (login){
  user.forEach(element => {
    if(element.login === login){
      document.getElementById('userReview').innerHTML = `${element.review}`;
      document.getElementById('userName').innerHTML = `${element.name}`;
      document.getElementById('userDirection').innerHTML = `${element.position}`;
    }
  });
}

function checkCarouselBtn(btn){
  selectedAvatar.classList.remove('active');
if(btn === 'nextBtn'){
  avatarOrder < 3 ? avatarOrder++ : avatarOrder = 0;
}
if(btn === 'previousBtn'){
 avatarOrder > 0 ? avatarOrder-- : avatarOrder = 3;
}
changeAvatarByBtn(avatarOrder)
}

function changeAvatarByBtn (value) {
  selectedAvatar = document.querySelector(`[data-order=\'${value}\']`).closest('.avatar-small')
  selectedAvatar.classList.add('active');
  switchReview(selectedAvatar.firstElementChild.dataset.userName)
  switchLargeAvatar(selectedAvatar.closest('.avatar-small'));
}


switchReview(selectedAvatar.firstElementChild.dataset.userName);
switchLargeAvatar(selectedAvatar.closest('.avatar-small'));
trimm (currentWorckCategory);
trimm (document.querySelectorAll('.masonry-brick'));



