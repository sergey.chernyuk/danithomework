
function masonry(grid, gridCell, gridGutter, dGridCol) {
  let g = document.querySelector(grid),
      gc = document.querySelectorAll(gridCell),
      gcLength = gc.length,
      gHeight = 0,
      i;
  
  for(i=0; i<gcLength; ++i) {
    gHeight+=gc[i].offsetHeight+parseInt(gridGutter);
  }
  g.style.height = gHeight/dGridCol + gHeight/(gcLength+1) + "px";  
}

const masonryGrid = document.querySelector('.masonry');
masonryGrid.insertAdjacentHTML("afterend", "<div class='masonry-preloader'>Loading...</div>");
const masonryPreloader = document.querySelector('.masonry-preloader');


["resize", "load"].forEach(function(event) {
  // Adding little preloader information
  masonryGrid.style.display="none";
  window.addEventListener(event, function() {
    imagesLoaded( document.querySelector('.masonry'), function() {
      masonryGrid.style.display="flex";
      masonryPreloader.style.display="none";
      // A masonry grid with 8px gutter, with 3 columns on desktop
      masonry(".masonry", ".masonry-brick", 8, 3);
    });
  }, false);
});
