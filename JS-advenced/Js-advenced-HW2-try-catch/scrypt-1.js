/*
Задание
Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.
*/

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const verifiableProperties = ["name", "author", "price"];
const container = document.querySelector("#root");

function checkProperties(obj) {
  for (const property of verifiableProperties) {
    if (!(property in obj)) {
      throw new Error(
        `В обьекте ${JSON.stringify(obj)} отсутствует Свойство "${property}"`
      );
    }
  }
  return true;
}

function getBooks() {
  return books.filter((element) => {
    try {
      return checkProperties(element);
    } catch (error) {
      console.log(error.message);
    }
  });
}

function createBookList(parent, array) {
  const ul = document.createElement("ul");
  parent.appendChild(ul);

  array.forEach((book) => {
    ul.insertAdjacentHTML(
      "beforeend",
      `<li style="margin-bottom: 15px">${book.author}: "${book.name}" <div>Цена: ${book.price} грн.</div></li>`
    );
  });
}

createBookList(container, getBooks());
