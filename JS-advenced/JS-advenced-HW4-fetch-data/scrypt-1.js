/*
Задание
Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

Технические требования:

Отправить AJAX запрос по адресу https://ajax.test-danit.com/api/swapi/films и получить список всех фильмов серии Звездные войны

Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. 
Список персонажей можно получить из свойства characters.
Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. 
Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.


Необязательное задание продвинутой сложности

Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. 
Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.
*/

const list = document.createElement("ul");
document.body.append(list);

const requestURL = "https://ajax.test-danit.com/api/swapi/films";


fetch(requestURL)
  .then((response) => response.json())
  .then((data) => renderList(data));

function createList(array, element) {
  element.innerHTML = `  <p>Episode: ${array.episodeId}</p>
                        <p>Title: ${array.name}</p>
                        <p>Opening crawl:  <span style="font-style: italic"> ${array.openingCrawl}</span></p>`;
}

function createCharsctersList(text, parrent) {
  const list = document.createElement("li");
  list.style.listStyle="none"
  list.innerHTML = `<p>Characters: <span style="font-style: italic;">${text}</span></p>`;
  parrent.appendChild(list);
}

function renderList(array) {
  array.forEach((item) => {
    const film = document.createElement("li");
    let characterList = "";

    (async () => {
      for (let i = 0; i < item.characters.length; i++) {
        const response = await fetch(item.characters[i]);
        const data = await response.json();
        const name = await data.name;

        characterList += `${name}`;
        if (i !== item.characters.length - 1) {
          characterList += `, `;
        }
      }

      createCharsctersList(characterList, film);
       
    })();

    createList(item, film);
    list.appendChild(film);
  });
}
