/*
Задание
1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), 
    age (возраст), salary (зарплата). 
    Сделайте так, чтобы эти свойства заполнялись при создании объекта.
2. Создайте геттеры и сеттеры для этих свойств.
3. Создайте класс Programmer, который будет наследоваться от класса Employee, 
    и у которого будет свойство lang (список языков).
4. Для класса Programmer перезапишите геттер для свойства salary. 
    Пусть он возвращает свойство salary, умноженное на 3.
5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.
*/

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }

  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }

  //   employeeDetails() {
  //   return [this._name, this._age, this._salary];
  //   }
}

// let emp = new Employee('Sergey', 34, 2000000);
// console.log(emp.name);
// console.log(emp.employeeDetails());

class Programmer extends Employee {
  constructor({ name, age, salary, lang }) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(newValue) {
    this._salary = newValue;
  }

  get salary() {
    return this._salary * 3;
  }

  employeeDetails() {
    return [this.name, this.age, this.salary, this.lang];
  }
}

const programmer1 = new Programmer({
  name: "Rob",
  age: 25,
  salary: 1000,
  lang: ["HTML", "CSS", "JavaScript"],
});

const programmer2 = new Programmer({
  name: "Katya",
  age: 32,
  salary: 4000,
  lang: ["C++", "HTML", "Java", "CSS", "JavaScript"],
});

const programmer3 = new Programmer({
  name: "Sergey",
  age: 33,
  salary: 2000000,
  lang: {
    ru: false,
    ua: true,
    eng: true,
    de: false,
    programming: ["HTML", "CSS"],
  },
});

console.log(programmer1.employeeDetails());
console.log(programmer2.employeeDetails());
console.log(programmer3.employeeDetails());
