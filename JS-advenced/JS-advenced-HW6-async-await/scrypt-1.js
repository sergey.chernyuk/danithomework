/*
Написать программу "Я тебя по айпи вычислю"

Технические требования:

Создать простую HTML страницу с кнопкой Вычислить по IP.
По нажатию на кнопку - отправить 
AJAX запрос по адресу https://api.ipify.org/?format=json, 
получить оттуда IP адрес клиента.
Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ 
и получить информацию о физическом адресе.
Под кнопкой вывести на страницу информацию, 
полученную из последнего запроса - континент, страна, регион, город, 
район города.
Все запросы на сервер необходимо выполнить с помощью async await.
*/

const getIpURL = "https://api.ipify.org/?format=json";

const btn = document.querySelector(".button");
const conteiner = document.querySelector(".location-conteiner");

btn.addEventListener("click", () => {
  getIp();
});

function getIp() {
  (async () => {
    try {
      const response = await fetch(getIpURL);
      const data = await response.json();
      checkIp(data.ip);
    } catch (err) {
      alert(err);
    }
  })();
}

function checkIp(velue) {
  let checkIPUrl = `http://ip-api.com/json/${velue}`;
  (async () => {
    try {
      const response = await fetch(checkIPUrl);
      const data = await response.json();
      renderLocation(data);
    } catch (err) {
      alert(err);
    }
  })();
}

function renderLocation(object) {
    conteiner.innerHTML = `
    <h1>I FOUND YOU</h1>
    <p><span>continent:</span> ${object.timezone.split("/")[0]}</p>
    <p><span>сountry:</span> ${object.country}</p>
    <p><span>region:</span> ${object.regionName}</p>
    <p><span>city:</span> ${object.city}</p>
    <p><span>district:</span> ${object.zip}</p>
    <h2>Follow the White Rabbit</h2>
    `;
}
