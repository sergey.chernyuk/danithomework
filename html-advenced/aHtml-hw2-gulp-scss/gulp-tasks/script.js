let gulp = require("gulp");
let rename = require("gulp-rename");
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
let uglify = require('gulp-uglify-es').default;

exports.js = function () {
	return gulp.src("src/js/*.js")
		.pipe(sourcemaps.init())
        .pipe(concat('script.min.js'))
		.pipe(uglify())
		// .pipe(sourcemaps.write()) // Inline source maps.
		.pipe(sourcemaps.write("./maps")) // In this case: lib/maps/bundle.min.js.map
		.pipe(gulp.dest("build/js"));
};