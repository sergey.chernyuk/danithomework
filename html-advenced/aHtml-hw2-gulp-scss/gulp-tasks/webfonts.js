// const { src } = require("gulp");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");

exports.webFonts = () => {
  return gulp.src("src/webfonts/*")
  // .pipe(newer("build/webfonts"))
  // .pipe(imagemin({
  //   verbose: true
  // }))
  .pipe(gulp.dest('build/webfonts'))
};