/*
Задание
Реализовать переключение вкладок (табы) на чистом Javascript.

Технические требования:

В папке tabs лежит разметка для вкладок.Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
При этом остальной текст должен быть скрыт.В комментариях указано, какой текст должен отображаться для какой вкладки.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
При этом нужно, чтобы функция, написанная в джаваскрипте, из - за таких правок не переставала работать.
*/

// const tabsNameList = document.querySelectorAll('.tabs li');


// function hideTabsItem() {
//   document.querySelector('.tabs .active').classList.remove('active');
//   document.querySelectorAll('.tabs-content .active').forEach(element => {
//     element.classList.remove('active');
//   });
// };

// function showTabsItem(name) {
//   name.classList.add('active');
//   document.querySelectorAll(`[data-switchText="${name.dataset.switch}"]`).forEach(element => {
//     element.classList.add('active');
//   });
// };

// // функция показа выбраного таба
// function showTabs() {
//   tabsNameList.forEach(element => {
//     element.addEventListener('click', function () {
//       hideTabsItem();
//       showTabsItem(element);
//     });
//   });
// };

// showTabs();


let selectedTabs = document.querySelector('.tabs .active');;

const nameListConteiner = document.querySelector('.tabs');

nameListConteiner.addEventListener('click', (event)=>{
  switchTabs(event.target);
});


function switchTabs(tab) {
  if (selectedTabs) {
    selectedTabs.classList.remove('active');
  }
  
    selectedTabs = tab;
    selectedTabs.classList.add('active')
  document.querySelectorAll(`.tabs-content li`).forEach(element => {
    if(element.dataset.switchtext === selectedTabs.dataset.switch){
      element.classList.add('active');
    } else {
      element.classList.remove('active');
    }
  })
}
