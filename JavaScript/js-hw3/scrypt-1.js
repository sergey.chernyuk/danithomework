// Задание
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. 
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек(типа Jquery).

// Технические требования:
// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. 
// Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.


// Необязательное задание продвинутой сложности:
// После ввода данных добавить проверку их корректности. 
// Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа
// заново(при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


let userNumberFirst = prompt('Введите первое число');
let userNumberSecond = prompt('Введите второе число');

while (!checkValidNumber(userNumberFirst) || !checkValidNumber(userNumberSecond)) {
    userNumberFirst = prompt('Ошибка! Введите число 1');
    userNumberSecond = prompt('Введите число 2');
}

let userMathOperation = prompt('Введите необходимую операцию: +, -, *, /');

while (!checkValidMathOperation(userMathOperation)) {
    userMathOperation = prompt('Ошибка. Введите корректное значение операции: +, -, *, /');
}


function checkValidNumber(variable) {
    if (variable === "" || variable === null || Number.isNaN(+variable)) {
        return false;
    } else {
        return true;
    }
}

function checkValidMathOperation(operation) {
    if (operation === '*' || operation === '/' || operation === '+' || operation === '-') {
        return true;
    } else {
        return false;
    }
}

// function calc(value1, value2, operator) {
//     switch (operator) {
//         case '*':
//             return console.log(`${value1} * ${value2} = ${+value1 * +value2}`);
//         case '/':
//             return console.log(`${value1} / ${value2} = ${+value1 / +value2}`);
//         case '+':
//             return console.log(`${value1} + ${value2} = ${+value1 + +value2}`);
//         case '-':
//             return console.log(`${value1} - ${value2} = ${+value1 - +value2}`);
//     }
// }

// calc(userNumberFirst, userNumberSecond, userMathOperation);

function calc(value1, value2, operator) {
    switch (operator) {
        case '*':
            return `${value1} * ${value2} = ${+value1 * +value2}`;
        case '/':
            return `${value1} / ${value2} = ${+value1 / +value2}`;
        case '+':
            return `${value1} + ${value2} = ${+value1 + +value2}`;
        case '-':
            return `${value1} - ${value2} = ${+value1 - +value2}`;
    }
}

console.log(calc(userNumberFirst, userNumberSecond, userMathOperation));
