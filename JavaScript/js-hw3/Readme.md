#Теоретический вопрос

1.  Описать своими словами для чего вообще нужны функции в программировании.
2.  Описать своими словами, зачем в функцию передавать аргумент.

#Ответ

1. Функция - это минни программа. Для того что бы не писать каждый раз
   дублирующий код, мы можем написать функцию и при необходимости к ней
   обращаться.
2. Аргументами мы говорим функции с какими данными работать. По сути это те
   переменные, которые мы хотим обработать с помощью кода внутри функции для
   получения результата.
