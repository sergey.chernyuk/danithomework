/*
Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек(типа Jquery).

Технические требования:

Создать функцию, которая будет принимать на вход массив и опциональный 
второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
Каждый из элементов массива вывести на страницу в виде пункта списка;
Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;

Примеры массивов, которые можно выводить на экран:
["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];

Можно взять любой другой массив.

Необязательные задания продвинутой сложности:

Добавьте обработку вложенных массивов. Если внутри массива одним из элементов 
будет еще один массив, выводить его как вложенный список.
Пример такого массива:
["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
*/


const userArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin", "Буча", "Бровары"], "Odessa", "Lviv",["Моршин", "Городок", "Броды", "Красноград"], "Dnieper"];


function getNewArray(oldArray) {
    return `<ul> ${oldArray.map(element => {
        if (Array.isArray(element)) {
           return getNewArray(element);
        } else {
            return `<li>${element}</li>`;
        }
    }).join('')
        }</ul>`;
    }

function showList(array, parent = document.body) {
    parent.insertAdjacentHTML("afterBegin", getNewArray(array));
}

showList(userArray);



function timer(from, to) {
    let current = from;

    setTimeout(function go() {
        if (to == current) {
            document.documentElement.innerHTML = '';
        } else {
             let target = document.querySelector("ul");
            target.insertAdjacentHTML("afterEnd", `<p>${current}</p>`);
            if (current > to) {
                setTimeout(go, 1000);
            }
            current--;
        }}, 1000)
}

timer(3, 0)
