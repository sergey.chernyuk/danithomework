/*
Задание
Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript,
 без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Взять любое готовое домашнее задание по HTML/CSS.
Добавить на макете кнопку "Сменить тему".
При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. 
При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
Выбранная тема должна сохраняться и после перезагрузки страницы
 */

const page = document.getElementById("document");
page.setAttribute("data-theme", localStorage.getItem("Theme"));

document.addEventListener("DOMContentLoaded", function (event) {
  let themeSwitcher = document.getElementById("theme-switcher");

  themeSwitcher.addEventListener("click", () => {
    let currentTheme = page.getAttribute("data-theme");
    currentTheme === "dark" ? page.setAttribute("data-theme", "light"): page.setAttribute("data-theme", "dark");
    localStorage.setItem("Theme", currentTheme === "dark" ? "light" : "dark");
  });
});
