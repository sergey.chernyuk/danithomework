/*
1. Написать функцию для подсчета n-го обобщенного числа Фибоначчи.
Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности
(могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти.
Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
2. Считать с помощью модального окна браузера число, которое введет пользователь (n).
2. С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
*/

function checkValidNumber(variable) {
    if (!Number.isInteger(variable)) {
        return false;
    } else {
        return true;
    }
}

let n = +prompt('Введите порядковый номер числа Фибоначи n:');

while (!checkValidNumber(n)) {
    n = +prompt('Ошибка! Введите число');
}


// function fibonacci(numero) {
//     if (Math.sign(numero) == 1) {
//         let f0 = 1;
//         let f1 = 1;
//
//         for (let i = 3; i <= numero; i++) {
//             let c = f0 + f1;
//             f0 = f1;
//             f1 = c;
//         }
//         return f1;

//     } else if (Math.sign(numero) == -1) {
//         let f0 = -1;
//         let f1 = -1;
//
//         for (let i = -3; i >= numero; i--) {
//             let c = f1 + f0;
//             f0 = f1;
//             f1 = c;
//         }
//         return f1;
//     } else {
//         return numero;
//     }
// }


function fibonacci(numero) {
    if (Math.sign(numero) == 1) {
        if (numero <= 2) return 1;
        return fibonacci(numero - 1) + fibonacci(numero - 2)
    } else if (Math.sign(numero) == -1) {
        if (numero == -1) return 1;
        if (numero == -2) return -1;
        return fibonacci(numero + 2 ) - fibonacci(numero + 1)
    }else {
        return numero;
    }
}



console.log(fibonacci(n));