#Теоретический вопрос

1.  Описать своими словами в несколько строчек, зачем в программировании нужны
    циклы.

#Ответ

1.  Циклы, как следует из названия, нужны для того что бы наш код выполнялся
    пока заданное условие не будет выполнено. Или если нам необходимо что бы код
    повторялся заданное количество итераций. Это даёт возможность не писать
    сотни раз if с одним и тем же действием.
