//переход по якорю
$(document).ready(function () {
  $('#navMenu').on('click', 'a', function (event) {
    event.preventDefault(); //отменяем стандартную обработку нажатия по ссылке
    var id = $(this).attr('href'), //считываем значение href
      top = $(id).offset().top; //узнаем высоту от начала страницы до блока на который ссылается якорь
    $('body,html').animate({ scrollTop: top }, 1700); //задаём скорость перемещения 1700 мс
  });
});

//Показ кнопки "наверх" и функция по клику на неё
$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > $(window).height()) {
      //если промотали больше видимой высоты браузера то..
      $('#scroll').fadeIn(); //плавно показать елемент с id=scroll
    } else {
      $('#scroll').fadeOut(); //плавно убрать елемент с id=scroll
    }
  });
  $('#scroll').click(function () {
    // по клику на елемент с id=scroll
    $('html, body').animate({ scrollTop: 0 }, 1000); //пролистать к позиции 0 со скоростью 1000мс
    return false;
  });
});

//свёртывание и развёртование блока
$('#rollBtn').click(function () {
  //по клику на елемент с id=rollBtn
  $('.masonry').slideToggle('slow'); //свернуть/развернуть ел-нт по селектору .masonry с медленной скоростью
});
