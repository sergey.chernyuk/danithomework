// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
// Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? 
    // и кнопками Ok, Cancel.Если пользователь нажал Ok, показать на экране сообщение: Welcome, + имя пользователя.
    // Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
// Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

    // После ввода данных добавить проверку их корректности.Если пользователь не ввел имя,
    // либо при вводе возраста указал не число - спросить имя и возраст заново
    // (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).



let userName = prompt('Enter your name');

while (!userName) {
    userName = prompt('Enter your name');
    }

let userAge = +prompt('Enter your age');

while (!Number.isInteger(userAge)) {
        userAge = +prompt('You did\'t enter an age. Enter your age');
    }

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (18 <= userAge && userAge <= 22) {
    let userUnsver = confirm('Are you sure you want to continue?');
    userUnsver ? alert(`Welcome ${userName}`) : alert('You are not allowed to visit this website');
} else {
    alert(`Welcome ${userName}`);
}
