/*
Задание
Реализовать функцию для создания объекта "пользователь". 
Задача должна быть реализована на языке javascript, 
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, 
соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). 
Вывести в консоль результат выполнения функции.


Необязательное задание продвинутой сложности:

Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. 
Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.
*/

function isValidString(value) {
    if (!value) {//проверка на пустую строку и отмену
        return false;
    } else if (/[^\u0020-\u007F\u00A0-\u024F\u1E00-\u1EFF]/.test(value)) { //проверка на латиницу
        return false;
    } else {
        return true;
    }
}

function createNewUser(firstName = null, lastName = null) {
    return {
        firstName,
        lastName,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
    }
}


let userFirstName = prompt('Введите Ваше имя латиницей:');
while (!isValidString(userFirstName)) {
    userFirstName = prompt('Введите Ваше имя латиницей!!!')
}

let userLastName = prompt('Введите Вашу Фамилию латиницей:');
while (!isValidString(userLastName)) {
    userLastName = prompt('Введите Вашу Фамилию латиницей!!!')
}


const newUser = createNewUser(userFirstName, userLastName);

console.log(newUser.getLogin());

