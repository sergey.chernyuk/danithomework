/*
Задание
Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, 
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

При загрузке страницы показать пользователю поле ввода (input) с надписью Price. 
Это поле будет служить для ввода числовых значений
Поведение поля должно быть следующим:

При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
Когда убран фокус с поля - его значение считывается, над полем создается span, 
в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка 
с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, 
под полем выводить фразу - Please enter correct price. span со значением при этом не создается.

В папке img лежат примеры реализации поля ввода и создающегося span.
*/

const getElement = document.getElementsByName('getPrice')[0];

const getRemoveBtn = () => {
    return `<span class="remove-button" onclick="removeItem(this)">x</span>`;
};

const showError = () => {
    getElement.classList.add("input-price-danger");
    document.querySelector('.danger-text').style.display = 'block';
};

const hideError = () => {
    getElement.classList.remove("input-price-danger");
    document.querySelector('.danger-text').style.display = ''
};

function createNewElement(price) {
    return `<span class="current-price">Текущая цена: ${price} ${getRemoveBtn()} </span>`
}

function removeItem(element) {
    element.closest('.current-price').remove();
}

function isValidValue(velue) {
    return !velue ? false : true
}

function addNewElement() {
    const inputValue = document.getElementsByName('getPrice')[0].value;

    if (isValidValue(inputValue) && inputValue >= 0) {
        hideError();
        getElement.insertAdjacentHTML("beforebegin", createNewElement(inputValue));
        document.querySelector("input").value = "";
    } else if (inputValue < 0) {
        showError();
    }
}


getElement.onblur = addNewElement;

