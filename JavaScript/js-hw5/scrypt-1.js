/*
Задание
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. 
Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*/

function isValidString(value) {
    if (!value) {//проверка на пустую строку и отмену
        return false;
    } else if (/[^\u0020-\u007F\u00A0-\u024F\u1E00-\u1EFF]/.test(value)) { //проверка на латиницу
        return false;
    } else {
        return true;
    }
}



function createNewUser(firstName = null, lastName = null, birthday= null) {
    return {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge() {
            const birthDay = +this.birthday.slice(0, 2);
            const birthMonth = +this.birthday.slice(3, 5) - 1;
            const birthYear = +this.birthday.slice(-4);
            const today = new Date();

            let userAge = today.getFullYear() - birthYear;
  
            today.getMonth() < birthMonth || (today.getDate() < birthDay && today.getMonth() == birthMonth) ? userAge-- : userAge;
            return userAge;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
            
        }
    }
}




let userFirstName = prompt('Введите Ваше имя латиницей:');
while (!isValidString(userFirstName)) {
    userFirstName = prompt('Введите Ваше имя латиницей!!!')
}

let userLastName = prompt('Введите Вашу Фамилию латиницей:');
while (!isValidString(userLastName)) {
    userLastName = prompt('Введите Вашу Фамилию латиницей!!!')
}

let usersDateOfBirth = prompt('Введите дату Вашего рождения в формате dd.mm.yyyy')



const newUser = createNewUser(userFirstName, userLastName, usersDateOfBirth);

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());